package com.sinosoft.core.base;

import cn.hutool.db.Page;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;


public abstract  class BaseServiceImp<T,String>  implements  BaseService<T,String>{



    /**
     * 在子类实现此函数,为下面的CRUD操作提供DAO.
     */
    protected abstract BaseDao<T, java.lang.String> getEntityDao();


    @Override
    public long total() {
        return getEntityDao().allCount();
    }

    @Override
    public void save(T t) {
        getEntityDao().insert(t);
    }

    @Override
    public void update(T t) {
        getEntityDao().updateById(t);
    }


    public T findById(String id) {
        return  getEntityDao().unique(id);
    }


    public String loadFormData(String id) {
        return null;
    }

    @Override
    public List<T> queryAllData(T query) {
        return  getEntityDao().all();
    }

    @Override
    public List<T> pagedQueryByBean(int start, int limit, T query) {
        return getEntityDao().all(start, limit);
    }

    /**
     * 根据Id（主键删除数据的方法）
     * @param id
     */

    public void deletById(String id) {
        getEntityDao().deleteById(id);
    }
}
