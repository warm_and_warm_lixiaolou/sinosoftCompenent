package com.sinosoft.core.base;


import org.beetl.sql.core.mapper.BaseMapper;

import java.io.Serializable;

/**
 *基类Dao 接口
 * @param <T>
 */
public interface BaseDao<T, PK extends Serializable> extends BaseMapper<T> {

}
