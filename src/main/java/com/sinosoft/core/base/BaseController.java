package com.sinosoft.core.base;


import com.sinosoft.core.util.Page;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * controller的基类
 * @param <T>
 * @param <PK>
 */
public abstract class BaseController<T,PK extends String> {

    /**
     * 获取baseService
     * @return
     */
    public abstract BaseService<T, java.lang.String> getBaseService ();

    /**
     * 获取当前表总的记录数的方法
     * 访问路径为：/xxx/totalCount
     * @return
     */
    @RequestMapping("/totalCount")
    @ApiOperation(value = "通用的查询当前表的总记录数信息", notes = "")
    public long totalCount() {
        return getBaseService().total();
    }

    @RequestMapping("/save")
    @ApiOperation(value = "单表保存当前当前对象的信息,直接传入当前对象的各属性值")
    public void save(T t) {
        getBaseService().save(t);
    }

    /**
     * 更新基本信息表的方法
     * 访问路径为：/xx/update
     * @param t 当前对象，通过springMvc自动注入
     */
    @RequestMapping("/update")
    @ApiOperation(value = "通用的更新信息的方法", notes = "")
    public void update(T t) {
        getBaseService().update(t);
    }

    /**
     * 根据Id查询当前对象
     *访问路径：/xx/findById
     * @param id 主键
     * @return
     */
    @RequestMapping("/findById")
    @ApiOperation(value = "通用的根据Id查询当前对象的方法", notes = "")
    @ApiImplicitParam(name = "id", value = "主键Id", paramType = "path", required = true, dataType = "String")
    public T findById(String id) {
        return  getBaseService().findById(id);
    }

    /**
     * 根据Id查询当前对象，并生成form表单对应json的数据格式
     * 访问路径：/xx/loadFormData
     * @param id 主键Id
     * @return
     */
    @ApiOperation(value = "通用的根据Id查询当前对象的方法，生成form表单对应的数据", notes = "")
    @ApiImplicitParam(name = "id", value = "主键Id", paramType = "path", required = true, dataType = "String")
    @RequestMapping("/loadFormData")
    public String loadFormData(String id) {
        return getBaseService().loadFormData(id);
    }

    /**
     * 根据对象查询对应的数据
     * 访问路径：/xx/queryAllData
     * @param query 查询条件
     * @return
     */
    @RequestMapping("/queryAllData")
    @ApiOperation(value = "通用的跟进对象作为查询条件，查询当前表数据的方法", notes = "")
    public List<T> queryAllData(T query) {
        return  getBaseService().queryAllData(query);
    }

    /**
     * 分页查询的方法，并根据对象的条件进行查询
     * 访问路径：/xx/pagedQueryByBean
     * @param pageIndex
     * @param pageSize
     * @param query
     * @return
     */
    @RequestMapping("/pagedQueryByBean")
    @ApiOperation(value = "通用分页查询的方法", notes = "")

    public Page<T> pagedQueryByBean(int pageIndex, int pageSize, T query) {
        System.out.print(pageIndex + "___________" + pageSize);
        List<T> list = getBaseService().pagedQueryByBean(pageIndex, pageSize, query);
        // int totalRow = list.size();
        int totalRow = (int) getBaseService().total();
        if (totalRow == 0) {
            return new Page<T>(new ArrayList<T>(0), pageIndex, pageSize, 0, 0);    // totalRow = 0;
        }

        int totalPage = (int) (totalRow / pageSize);
        if (totalRow % pageSize != 0) {
            totalPage++;
        }

        if (pageIndex > totalPage) {
            return new Page<T>(new ArrayList<T>(0), pageIndex, pageSize, totalPage, (int) totalRow);
        }

        Page<T> page = new Page<>(list, pageIndex, pageSize, totalPage, list.size());

        return page;
    }

    /**
     * 根据Id（主键删除数据的方法）
     * 访问路径：/xx/delete
     * @param id 主键Id
     * @return 执行成功返回true，请注意此值是个字符串，非boolean值
     */
    @GetMapping("/delete")
    @ApiOperation(value="根据主键Id，删除当前对象的信息，删除成功之后返回字符串true")
    @ApiImplicitParam(name = "id", value = "主键Id", paramType = "path", required = true, dataType = "String")
    public String deletById(String id) {
        getBaseService().deletById(id);
        return  "true";
    }


}
