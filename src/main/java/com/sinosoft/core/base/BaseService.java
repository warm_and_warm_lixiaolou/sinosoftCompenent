package com.sinosoft.core.base;

import cn.hutool.db.Page;


import java.util.List;

/**
 * baseService 基础类的接口
 * @param <T>
 */
public interface BaseService<T,PK> {

    /**
     * 查询总记录的方法
     * @return
     */
    public  long total() ;

    /**
     * 默认的保存的方法
     * @param t
     */
    public  void save(T t);

    /**
     * 默认的更新的方法
     * @param t
     */
    public   void update(T t );

    /**
     * 根据主键查询对应表的方法
     * @param id
     * @return
     */
    public   T findById(String id);

    /**
     * 根据主键Id加载Form表单的方法
     * @param id
     * @return
     */
    public String loadFormData(String id);

    /**
     * 查询全部数据的方法
     * @param query
     * @return
     */
    public List<T> queryAllData(T query);

    /**
     * 默认查询列表的方法
     * @param start
     * @param limit
     * @param query
     * @return
     */
    public List<T> pagedQueryByBean(int start, int limit, T query);

    /**
     * 根据Id删除对应数据的方法
     * @param id
     */
    public void deletById(String id);

}
