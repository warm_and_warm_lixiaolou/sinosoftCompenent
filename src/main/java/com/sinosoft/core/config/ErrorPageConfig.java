package com.sinosoft.core.config;


import org.springframework.boot.web.server.ConfigurableWebServerFactory;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;


import java.util.HashSet;
import java.util.Set;

/**
 * 错误代码指向对应的html页面
 */
@Configuration
public class ErrorPageConfig {
    @Bean
    public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer() {
        return new WebServerFactoryCustomizer<ConfigurableWebServerFactory>() {
            @Override
            public void customize(ConfigurableWebServerFactory factory) {

                ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/error/404.html");

                /*Set errorSet = new HashSet();
                errorSet.add(error404Page);*/
                factory.addErrorPages(error404Page);
                // factory.setErrorPages(errorSet);
            }
        };
    }


}
