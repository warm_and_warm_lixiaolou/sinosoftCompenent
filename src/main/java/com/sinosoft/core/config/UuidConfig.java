package com.sinosoft.core.config;

import com.ibeetl.starter.BeetlSqlCustomize;
import com.sinosoft.core.util.UuidUtil;
import org.beetl.sql.core.IDAutoGen;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.ext.spring4.SqlManagerFactoryBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义主键的生成策略
 */
@Configuration
public class UuidConfig {
    @Bean
    public BeetlSqlCustomize beetlSqlCustomize(){
        return  new BeetlSqlCustomize(){
            public void customize(SqlManagerFactoryBean sqlManagerFactoryBean){
                //....
                SQLManager sqlManager= null;
                try {
                    sqlManager = sqlManagerFactoryBean.getObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                sqlManager.addIdAutonGen("uuid20", new IDAutoGen(){
                    @Override
                    public Object nextID(String params) {
                        return UuidUtil.getUUID20();
                    }
                });

            }
        };
    }
}
