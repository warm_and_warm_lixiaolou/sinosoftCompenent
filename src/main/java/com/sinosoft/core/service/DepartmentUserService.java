package com.sinosoft.core.service;


import com.sinosoft.core.base.BaseService;
import com.sinosoft.core.domain.DepartmentUser;


/**
 * 部门用户管理的service接口
 */
public interface DepartmentUserService extends BaseService<DepartmentUser,String> {
    /**
     * 根据用户名和密码校验用户是否存在的方法
     * @param personEname
     * @param userPwd
     * @return
     */
    public int chekUserPwd(String personEname,String userPwd);
}
