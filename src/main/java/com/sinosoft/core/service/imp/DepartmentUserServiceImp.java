package com.sinosoft.core.service.imp;


import cn.hutool.db.Page;
import com.alibaba.fastjson.JSONObject;
import com.sinosoft.core.base.BaseDao;
import com.sinosoft.core.base.BaseServiceImp;
import com.sinosoft.core.dao.DepartmentUserDao;
import com.sinosoft.core.domain.DepartmentUser;
import com.sinosoft.core.service.DepartmentUserService;
import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 实现类
 */
@Service
public class DepartmentUserServiceImp extends BaseServiceImp<DepartmentUser,String> implements DepartmentUserService    {

    @Autowired
    SQLManager sqlManager;

    @Autowired
    private DepartmentUserDao departmentUserDao;


    @Override
    protected BaseDao<DepartmentUser, String> getEntityDao() {
        return departmentUserDao;
    }

    /**
     * 根据用户和密码校验用户是否存在的方法
     * @param personEname
     * @param userPwd
     * @return
     */
    @Override
    public int chekUserPwd(String personEname, String userPwd) {
        return departmentUserDao.chekUserPwd(personEname,userPwd);

    }
}
