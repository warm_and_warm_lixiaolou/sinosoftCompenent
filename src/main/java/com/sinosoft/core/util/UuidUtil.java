package com.sinosoft.core.util;
 
import java.util.UUID;

/**     
 *  生成随机数字，主要用于生成主键
 * @ClassName：com.sinosoft.sinosoftHelp.util.string.UuidHelper     
 * @author: lixl  
 * @date: 2014年4月16日 下午2:23:49      
 * @version: V1.0    
 */

public class UuidUtil {

	/**
	 *  生成20位不重复随机数的方法
	 * @Title: getUUID
	 * @author: lixl  
	 * @date: 2014年4月16日 下午2:44:26 
	 * @return 生成的20位的随机数字
	 */
	public static String getUUID20() {
		String s = UUID.randomUUID().toString();
		// 去掉“-”符号
		return (s.substring(0, 8) + s.substring(9, 13) + s.substring(14, 18)
				+ s.substring(19, 23) + s.substring(24)).substring(0, 20);
	}

	/**
	 *  生成32位不重复随机数的方法
	 * @Title: generate32
	 * @author: lixl  
	 * @date: 2014年5月21日 上午10:28:27 
	 * @return 生成32位不重复随机数
	 */
	public static String generate32() {
		return generate36().replace("-", "");
	}

	/**
	 * 生成36位不重复随机数的方法
	 * @Title: generate36
	 * @author: lixl  
	 * @date: 2014年5月21日 上午10:28:20 
	 * @return 生成36位不重复随机数
	 */
	public static String generate36() {
		return UUID.randomUUID().toString();
	}
}
