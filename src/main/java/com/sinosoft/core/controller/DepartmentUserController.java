package com.sinosoft.core.controller;


import com.sinosoft.core.base.BaseController;
import com.sinosoft.core.base.BaseService;
import com.sinosoft.core.domain.DepartmentUser;
import com.sinosoft.core.service.DepartmentUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *部门用户管理的controller
 */
@RestController
@RequestMapping("/departmentUser")
@Api("用户管理的Api文档")
public class DepartmentUserController extends BaseController<DepartmentUser,String> {

    @Autowired
    private DepartmentUserService departmentUserService;

    @Override
    public BaseService<DepartmentUser, String> getBaseService() {
        return departmentUserService;
    }



    /**
     * 校验用户是否存在
     * @param personEname
     * @param userPwd
     * @return
     */


    @GetMapping("/checkUser")
    @ApiOperation(value = "根据用户名和密码校验当前用户信息是否存在的方法")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "personEname", value = "用户登录英文名称", required = true, paramType = "path", dataType = "String"),
            @ApiImplicitParam(name = "userPwd", value = "用户密码", paramType = "path", required = true, dataType = "String")})
    public int checkUser(String personEname,String userPwd){
       return  departmentUserService.chekUserPwd(personEname,userPwd);
    }


}
