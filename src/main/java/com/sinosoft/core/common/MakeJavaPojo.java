package com.sinosoft.core.common;

import cn.hutool.core.io.resource.ClassPathResource;
import org.beetl.sql.core.*;
import org.beetl.sql.core.db.DBStyle;
import org.beetl.sql.core.db.MySqlStyle;
import org.beetl.sql.ext.DebugInterceptor;
import org.beetl.sql.ext.gen.GenConfig;
import org.beetl.sql.ext.gen.MapperCodeGen;

import java.io.IOException;
import java.util.Properties;

/**
 * 生成pojo的工具类
 */
public class MakeJavaPojo {
    /**
     * 生成SQLManager的方法
     */
    public static  SQLManager configSqlManager() throws IOException {
        ClassPathResource resource = new ClassPathResource("application.properties");
        Properties properties = new Properties();
        properties.load(resource.getStream());

        ConnectionSource source = ConnectionSourceHelper.getSimple(properties.getProperty("spring.datasource.driverClassName"),
                properties.getProperty("spring.datasource.url"), properties.getProperty("spring.datasource.username"), properties.getProperty("spring.datasource.password"));
        DBStyle mysql = new MySqlStyle();
        // sql语句放在classpagth的/sql 目录下
        SQLLoader loader = new ClasspathLoader("/sql");
        // 数据库命名跟java命名一样，所以采用DefaultNameConversion，还有一个是UnderlinedNameConversion，下划线风格的，
        UnderlinedNameConversion nc = new  UnderlinedNameConversion();
        // 最后，创建一个SQLManager,DebugInterceptor 不是必须的，但可以通过它查看sql执行情况
        SQLManager sqlManager = new SQLManager(mysql,loader,source,nc,new Interceptor[]{new DebugInterceptor()});
        return sqlManager;
    }

    /**
     * main方法执行pojo生成的方法
     * @param args
     */
    public static void main (String args[]) throws  Exception{
        SQLManager sqlManager = configSqlManager();
      //  sqlManager.genPojoCode("department_user","com.sinosoft.core.domain");
        sqlManager.genSQLTemplateToConsole("department_user",null);
        //MapperCodeGen mapper = new MapperCodeGen("com.sinosoft.core.dao");
        //GenConfig config = new GenConfig();
        //config.codeGens.add(mapper);
       // sqlManager.gen("department_user", config);
    }
}
