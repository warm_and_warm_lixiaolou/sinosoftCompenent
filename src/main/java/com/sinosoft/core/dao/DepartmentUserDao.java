package com.sinosoft.core.dao;

import com.sinosoft.core.base.BaseDao;
import com.sinosoft.core.domain.DepartmentUser;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.annotatoin.SqlStatement;
import org.beetl.sql.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.io.Serializable;


/**
 *userDao
 */
@SqlResource("department_user")
@Repository
public interface DepartmentUserDao extends BaseDao<DepartmentUser,String> {
    @SqlStatement(params = "personEname,userPwd")
    public int chekUserPwd(String personEname,String userPwd);
}
