package com.sinosoft.core.domain;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.Table;


/* 
* 
* gen by beetlsql 2018-09-10
*/
@Table(name="department_user")
public class DepartmentUser   {
	
	private String id ;
	private Integer loginCount ;
	private String answer ;
	private String deptId ;
	private String invalidFlag ;
	private String organizationDeptOid ;
	private String personCname ;
	private String personEname ;
	private String questions ;
	private String userDescription ;
	private String userType ;
	private String userPwd ;
	private String email ;
	private String mobile ;
	private String regionCode ;
	private String regionName ;
	private String roleId ;
	private Date createDate ;
	private Date invalidDate ;
	private Date modifyDate ;
	private Date lastLoginTime ;
	private Date loginTime ;
	
	public DepartmentUser() {
	}

	@AssignID("uuid20")
	public String getId(){
		return  id;
	}
	public void setId(String id ){
		this.id = id;
	}
	
	public Integer getLoginCount(){
		return  loginCount;
	}
	public void setLoginCount(Integer loginCount ){
		this.loginCount = loginCount;
	}
	
	public String getAnswer(){
		return  answer;
	}
	public void setAnswer(String answer ){
		this.answer = answer;
	}
	
	public String getDeptId(){
		return  deptId;
	}
	public void setDeptId(String deptId ){
		this.deptId = deptId;
	}
	
	public String getInvalidFlag(){
		return  invalidFlag;
	}
	public void setInvalidFlag(String invalidFlag ){
		this.invalidFlag = invalidFlag;
	}
	
	public String getOrganizationDeptOid(){
		return  organizationDeptOid;
	}
	public void setOrganizationDeptOid(String organizationDeptOid ){
		this.organizationDeptOid = organizationDeptOid;
	}
	
	public String getPersonCname(){
		return  personCname;
	}
	public void setPersonCname(String personCname ){
		this.personCname = personCname;
	}
	
	public String getPersonEname(){
		return  personEname;
	}
	public void setPersonEname(String personEname ){
		this.personEname = personEname;
	}
	
	public String getQuestions(){
		return  questions;
	}
	public void setQuestions(String questions ){
		this.questions = questions;
	}
	
	public String getUserDescription(){
		return  userDescription;
	}
	public void setUserDescription(String userDescription ){
		this.userDescription = userDescription;
	}
	
	public String getUserType(){
		return  userType;
	}
	public void setUserType(String userType ){
		this.userType = userType;
	}
	
	public String getUserPwd(){
		return  userPwd;
	}
	public void setUserPwd(String userPwd ){
		this.userPwd = userPwd;
	}
	
	public String getEmail(){
		return  email;
	}
	public void setEmail(String email ){
		this.email = email;
	}
	
	public String getMobile(){
		return  mobile;
	}
	public void setMobile(String mobile ){
		this.mobile = mobile;
	}
	
	public String getRegionCode(){
		return  regionCode;
	}
	public void setRegionCode(String regionCode ){
		this.regionCode = regionCode;
	}
	
	public String getRegionName(){
		return  regionName;
	}
	public void setRegionName(String regionName ){
		this.regionName = regionName;
	}
	
	public String getRoleId(){
		return  roleId;
	}
	public void setRoleId(String roleId ){
		this.roleId = roleId;
	}


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getCreateDate(){
		return  createDate;
	}
	public void setCreateDate(Date createDate ){
		this.createDate = createDate;
	}

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getInvalidDate(){
		return  invalidDate;
	}
	public void setInvalidDate(Date invalidDate ){
		this.invalidDate = invalidDate;
	}

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getModifyDate(){
		return  modifyDate;
	}
	public void setModifyDate(Date modifyDate ){
		this.modifyDate = modifyDate;
	}

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getLastLoginTime(){
		return  lastLoginTime;
	}
	public void setLastLoginTime(Date lastLoginTime ){
		this.lastLoginTime = lastLoginTime;
	}

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getLoginTime(){
		return  loginTime;
	}
	public void setLoginTime(Date loginTime ){
		this.loginTime = loginTime;
	}
	

}
