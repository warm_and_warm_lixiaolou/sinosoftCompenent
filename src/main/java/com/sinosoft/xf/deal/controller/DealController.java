package com.sinosoft.xf.deal.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller类测试
 */
@RestController
public class DealController {
    @RequestMapping("/hello")
    public String say() {
        return "helloWord";
    }
}
