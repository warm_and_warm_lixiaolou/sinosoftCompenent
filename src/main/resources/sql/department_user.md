sample
===
* 注释

	select #use("cols")# from department_user  where  #use("condition")#

cols
===
	id,Person_EName,Person_CName,User_Pwd,Create_Date,Modify_Date,Invalid_Date,Invalid_Flag,Answer,Questions,mobile,email,USER_TYPE,DEPT_ID,USER_Description,region_code,region_name,login_time,last_login_time,login_count,role_id,ORGANIZATION_DEPT_OID

updateSample
===
	
	id=#id#,Person_EName=#personEname#,Person_CName=#personCname#,User_Pwd=#userPwd#,Create_Date=#createDate#,Modify_Date=#modifyDate#,Invalid_Date=#invalidDate#,Invalid_Flag=#invalidFlag#,Answer=#answer#,Questions=#questions#,mobile=#mobile#,email=#email#,USER_TYPE=#userType#,DEPT_ID=#deptId#,USER_Description=#userDescription#,region_code=#regionCode#,region_name=#regionName#,login_time=#loginTime#,last_login_time=#lastLoginTime#,login_count=#loginCount#,role_id=#roleId#,ORGANIZATION_DEPT_OID=#organizationDeptOid#

condition
===

	1 = 1  
	@if(!isEmpty(id)){
	 and id=#id#
	@}
	@if(!isEmpty(personEname)){
	 and Person_EName=#personEname#
	@}
	@if(!isEmpty(personCname)){
	 and Person_CName=#personCname#
	@}
	@if(!isEmpty(userPwd)){
	 and User_Pwd=#userPwd#
	@}
	@if(!isEmpty(createDate)){
	 and Create_Date=#createDate#
	@}
	@if(!isEmpty(modifyDate)){
	 and Modify_Date=#modifyDate#
	@}
	@if(!isEmpty(invalidDate)){
	 and Invalid_Date=#invalidDate#
	@}
	@if(!isEmpty(invalidFlag)){
	 and Invalid_Flag=#invalidFlag#
	@}
	@if(!isEmpty(answer)){
	 and Answer=#answer#
	@}
	@if(!isEmpty(questions)){
	 and Questions=#questions#
	@}
	@if(!isEmpty(mobile)){
	 and mobile=#mobile#
	@}
	@if(!isEmpty(email)){
	 and email=#email#
	@}
	@if(!isEmpty(userType)){
	 and USER_TYPE=#userType#
	@}
	@if(!isEmpty(deptId)){
	 and DEPT_ID=#deptId#
	@}
	@if(!isEmpty(userDescription)){
	 and USER_Description=#userDescription#
	@}
	@if(!isEmpty(regionCode)){
	 and region_code=#regionCode#
	@}
	@if(!isEmpty(regionName)){
	 and region_name=#regionName#
	@}
	@if(!isEmpty(loginTime)){
	 and login_time=#loginTime#
	@}
	@if(!isEmpty(lastLoginTime)){
	 and last_login_time=#lastLoginTime#
	@}
	@if(!isEmpty(loginCount)){
	 and login_count=#loginCount#
	@}
	@if(!isEmpty(roleId)){
	 and role_id=#roleId#
	@}
	@if(!isEmpty(organizationDeptOid)){
	 and ORGANIZATION_DEPT_OID=#organizationDeptOid#
	@}

chekUserPwd	
===
* 校验用户密码的方法

    select count(1) from department_user t where t.Person_EName=#personEname# 
    and t.User_Pwd=#userPwd#