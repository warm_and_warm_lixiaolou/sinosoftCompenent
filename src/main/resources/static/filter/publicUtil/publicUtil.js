// 区域下拉框
function configOwnereRegionCodeSelet(selectId) {
    var selector = $('#' + selectId);
    var codeType = selector.attr("codeType");
    if (selector) {
        $.ajax({
            url: Constant.SERVER_PATH + '/dataDictionaryCodeNode/queryCodeComboxByCodeType?codeType=' + codeType,
            async: false,
            cache: false,
            dataType: "text",
            success: function (data) {
                selector.append(data);
            }
        });
    }

}

//根据codeType 从码表中获取对应的下拉列表信息
function configDataDictionaryByCodeType(selectId) {
    var selector = $('#' + selectId);
    var codeType = selector.attr("codeType");
    if (selector) {
        $.ajax({
            url: Constant.SERVER_PATH + '/dataDictionaryCodeNode/queryCodeComboxByCodeType?codeType=' + codeType,
            async: false,
            cache: false,
            dataType: "text",
            success: function (data) {
                selector.append(data);
            }
        });
    }
}

layui.use(['upload'], function () {
    var upload = layui.upload;
    upload.render({
        elem: '#uploadImg'
        , url: '/cmpModule/lvshishiwuOrgInfo/uploadFile'
        , done: function (res, index, upload) { //上传后的回调
            var html = "<img id='" + res.fileRelationId + "'src='" + res.filePath + "'/>";
            $("#imgDiv").html(html);
        }
        , accept: 'file' //允许上传的文件类型
        //,size: 50 //最大允许上传的文件大小
        //,……
    });
});