layui.config({
    debug: false,//这样外部加载的节点，应该可以调用的到
    base: '/publicresource/src/js/'
});

layui.use(['form', 'jqueryExtend', 'laydate', 'tab'], function () {
    var $ = layui.jquery,
        //paging = layui.paging(),
        layerTips = parent.layer === undefined ? layui.layer : parent.layer, //获取父窗口的layer对象
        layer = layui.layer, //获取当前窗口的layer对象
        form = layui.form,
        table = layui.table,
        tab = layui.tab,
        laydate = layui.laydate;

    /**
     * 设置日期
     */
    var myDate = new Date();
    var month = '';
    if ((myDate.getMonth() + 1) < 10) {
        month = '0' + (myDate.getMonth() + 1);
    } else {
        month = (myDate.getMonth() + 1);
    }
    //给小于10的日加上0
    var day = '';
    if (myDate.getDate() < 10) {
        day = '0' + myDate.getDate();
    } else {
        day = myDate.getDate();
    }
    var startDate = myDate.getFullYear() + '-' + "01" + '-01';
    var endDate = myDate.getFullYear() + '-' + month + '-' + day;
    var defaultDate = startDate + " - " + endDate;

});

