layui.define('jquery',function(exports){ //提示：模块也可以依赖其它模块，如：layui.define('layer', callback);
  var $ = layui.jquery;
  	/**
	 * 序列化和反序列化表单字段。
	 * @param {Object} formdate
	 */
  $.fn.formSerialize =  function  (formdate) {
	    var element = $(this);
	    if (!!formdate) {
	        for (var key in formdate) {
	            var $field = element.find("[flagName=" + key + "]");
	            if ($field.length == 0) { 
	                continue;
	            }
	            var value = $.trim(formdate[key]).replace(/&nbsp;/g, '');
	            var type = $field.attr('type');
	            switch (type) {
	                case "checkbox":
	                    if (value == "true"||value=="1" ) {
	                        $field.attr("checked", 'checked');
	                    } else {
	                        $field.removeAttr("checked");
	                    }
	                    break;
	                case "select":
	                    $field.val(value).trigger("change");
	                    break;
	                case "radio":
	                	if(value){
	                		$field.each(function(){
	                		     if($(this).attr("value")==value){
	                		    	 $(this).attr("checked","true");
	                		    	 /*alert(123);
	                		    	 $(this).prop('checked', true); //form.render();
*/	                		     }else{
	                		    	 
	                		    	$(this).removeAttr("checked");
	                		    /*	 $(this).prop('checked', false); //form.render();
*/	                		     }
	                		  });
	                		
	                	}
	                	break;
	                default:
	                    $field.val(value);
	                    break;
	            }
	        };
	        return false;
	    }
	    var postdata = {};
	    element.find('input,select,textarea').each(function (r) {
	        var $this = $(this);
	        var id = $this.attr('id');
	        var type = $this.attr('type');
	        switch (type) {
	            case "checkbox":
	                postdata[id] = $this.is(":checked");
	                break;
	            default:
	                var value = $this.val() == "" ? "&nbsp;" : $this.val();
	                if (!$.getQueryString("id")) {
	                    value = value.replace(/&nbsp;/g, '');
	                }
	                postdata[id] = value;
	                break;
	        }
	    });
	    return postdata;
	}
  
  /**
   * 扩展其他jquery的方法
   */
  
  exports('jqueryExtend', null);
  
}); 
