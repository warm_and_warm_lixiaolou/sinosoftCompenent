layui.config({
    debug: false,//这样外部加载的节点，应该可以调用的到
    base: '/publicresource/src/js/'
});
var element = '';
layui.use(('element'), function () {
    element = layui.element;
});

function findHtmlContent(url) {
    var content = "空";
    $.ajax({
        type: 'get',
        async: false,
        url: url,//这里是url
        success: function (body, heads, status) {
            content = body;
        }
    });
    return content;
}

function tabAddExtend(layId, title, tabLayFilter, url, show, icon) {
    var htmlContent = findHtmlContent(url);
    element.tabAddExtend(tabLayFilter, {
        title: title
        , content: htmlContent//支持传入html
        , id: layId
        , show: show
        , icon: icon
    });
}

/**
 * 富文本编辑器  设置为只读
 * 富文本编辑器在数据回显的时候需要 重新 build一下
 */
var extend_Tool = {
    readOnlyTextarea: function (id) {
        $("iframe[textarea=" + id + "]").contents().find("body").attr("contenteditable", "false");
    }
}
