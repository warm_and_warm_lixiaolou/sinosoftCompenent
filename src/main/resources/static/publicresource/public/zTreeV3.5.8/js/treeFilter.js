// JavaScript Document
Namespace = new Object();
// 全局对象仅仅存在register函数，参数为名称空间全路径，如"Grandsoft.GEA" 
Namespace.register = function (fullNS, fn) {
    // 将命名空间切成N部分, 比如Grandsoft、GEA等
    var nsArray = fullNS.split('.');
    var sEval = "";
    var sNS = "";
    for (var i = 0; i < nsArray.length; i++) {
        if (i != 0) sNS += ".";
        sNS += nsArray[i];         // 依次创建构造命名空间对象（假如不存在的话）的语句
        // 比如先创建Grandsoft，然后创建Grandsoft.GEA，依次下去
        sEval += "if (typeof(" + sNS + ") == 'undefined') " + sNS + " = new Object();"
    }
    if (sEval != "") {
        var page = eval(sEval);
        if (fn && typeof fn == "function") {
            fn(page);
            document.onreadystatechange = luo.subSomething(page);//当页面加载状态改变的时候执行这个方法.
        }
    }
}


Namespace.register("luo");

//当页面加载状态改变的时候执行这个方法. 
luo.subSomething = function (page) {
    if (document.readyState == "complete") { //当页面加载状态
        if (page.onReady != undefined) page.onReady();
    } else {
        var arg = arguments;
        setTimeout(function () {
            arg.callee(page);
        }, 500);
    }
}

luo.ajax = function (url, data, type, async, successfn, errorfn) {
    $.ajax({
        url: url,
        type: type ? "GET" : "POST",
        data: data,
        async: async,
        dataType: 'text',
        success: function (rdata) {
            if (successfn && typeof successfn == "function") {
                successfn(rdata);
            }
        },
        error: function (msg) {
            if (errorfn && typeof errorfn == "function") {
                errorfn(msg);
            }
        }
    });
}
luo.Get = function (url, data, async, successfn, errorfn) {
    luo.ajax(url, data, true, async, successfn, errorfn);
}
luo.Post = function (url, data, async, successfn, errorfn) {
    luo.ajax(url, data, false, async, successfn, errorfn);
}

$.fn.Gztree = function (url, data, setting, successfn, errorfn) {
    var me = this;
    var zTreeObj;
    luo.Get(url, data, false, function (data) {
        var zNodes = eval("(" + data + ")");
        var rzTreeObj = $.fn.zTree.init(me, setting, zNodes);
        zTreeObj = rzTreeObj;
    }, function () {
        if (errorfn && typeof errorfn == "function") {
            errorfn('菜单加载异常!');
        }
    });
    if (successfn && typeof successfn == "function") {
        successfn(zTreeObj);
    }
    return zTreeObj;
}
$.fn.Pztree = function (url, data, setting, successfn, errorfn) {
    var me = this;
    var zTreeObj;
    luo.Post(url, data, false, function (data) {
        var zNodes = eval("(" + data + ")");
        var rzTreeObj = $.fn.zTree.init(me, setting, zNodes);
        zTreeObj = rzTreeObj;
    }, function () {
        if (errorfn && typeof errorfn == "function") {
            errorfn('菜单加载异常!');
        }
    });
    if (successfn && typeof successfn == "function") {
        successfn(zTreeObj);
    }
    return zTreeObj;
}
