# nunuanComponent

#### 项目介绍
组内沟通学习
本项目使用springBoot+beetlSql+layui进行封装，目前还在测试阶段，V1.0之后的版本可正式使用

#### 软件架构
软件架构说明
使用本项目之前建议大家，先看下如下两方面的知识：springBoot+beetlSql+layui2.4.3


#### 安装教程

1. 建议使用idea作为开发工具。
2. 建议使用git作为版本管理工具，集成git之后，从相应的地址把代码更新下来，使用之前找作者要相关的数据库脚本
3. 
###开发规范
1. 前端开发规范
1）开发的时候请保留目前公共的目录结构，方便后续进行升级完善。
2）前端框架基于layui2.4.3搭建的脚手架工程，请大家在开发的时候不要对源码修改，如果需要更改源码，请自定义样式和相关插件进行覆盖。
3）前端开发的时候务必注意，为了方便进行前后端分离，请大家在引用路径的时候，所有的资源必须引用相对路径，禁止引用绝对路径。
2. 后端开发规范

1） 目前此脚手架工程公共部分请保留，方便后续升级。
2）代码开发规范请参考阿里巴巴代码开发规约。
3）dao层禁止出现sql文件，请用md文件对sql进行管理。
4）如果对外提供restful风格的api接口，请使用swagger注释，方便集成人员进行集成，从而解决了再次编写对接文档的烦恼。


#### 使用说明

1. 本框架集成了swagger，进行api的管理，访问路径：http://127.0.0.1:8080/swagger-ui.html
2. 本框架集成了layui脚手架工程。
3. 建议前端都按照此目录机构配置，方便后续的升级
4. 目前此脚手架工程默认开启全系统跨域访问。
5. 静态页面统一放再resources/static 目录下，static目录相当于eclipse开发中的webRoot 目录

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)